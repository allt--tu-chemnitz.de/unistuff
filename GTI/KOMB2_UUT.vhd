library ieee;
use ieee.std_logic_1164.all;
use work.pack_2.all;

entity uut is
  port (EE_X : in  x01_vector(7 downto 0);
        EE_Y : out x01_vector(23 downto 16));
end uut;

architecture structure of uut is
  alias a  : x01 is EE_X(0);
  alias b  : x01 is EE_X(1);
  alias c  : x01 is EE_X(2);
  alias d  : x01 is EE_X(3);
  alias y1 : x01 is EE_Y(16);
  alias y2 : x01 is EE_Y(17);

component sn7400                                        
    port (x : in  X01_vector (1 to 2);
          y : out X01);       
  end component;
  
component sn7404
	port (x : in X01; y : out X01);
  end component;
  
component sn74151
	port (e   : in  X01;
        s   : in  X01_vector (2 downto 0);
        d   : in  X01_vector (0 to 7); 
        y,w : out X01);
  end component;
  
component sn74153
	port (e1,e2  : in  X01;
        s      : in  X01_vector (1 downto 0);
        d1,d2  : in  X01_vector (0 to 3);
        y1,y2  : out X01);
  end component;

signal n_d : X01;
signal n_c : X01;
signal const0 : X01 := '0';
signal const1 : X01;
signal blub : X01;
signal nd : X01_vector (1 to 4);
signal neg : X01_vector (1 to 4);

begin

u01: sn7404 port map(x=>d,y=>n_d);
u02: sn7404 port map(x=>c,y=>n_c);
u03: sn7404 port map(x=>const0,y=>const1);
u04: sn74151 port map(e=>const0,s(2)=>a,s(1)=>b,s(0)=>c,d(0)=>const0,d(1)=>n_d,d(2)=>n_d,d(3)=>const0,d(4)=>const1,d(5)=>n_d,d(6)=>n_d,d(7)=>const1,y=>y1,w=>blub);
u05: sn7400 port map(x(1)=>c,x(2)=>n_d,y=>nd(1));
u06: sn7400 port map(x(1)=>n_c,x(2)=>n_d,y=>nd(2));
u07: sn7400 port map(x(1)=>c,x(2)=>d,y=>nd(3));
u08: sn7400 port map(x(1)=>n_c,x(2)=>d,y=>nd(4));
u09: sn7404 port map(x=>nd(1),y=>neg(1));
u10: sn7404 port map(x=>nd(2),y=>neg(2));
u11: sn74153 port map(e1=>const0,e2=>const1,s(1)=>a,s(0)=>b,d1(0)=>neg(1),d1(1)=>neg(2),d1(2)=>nd(3),d1(3)=>nd(4),d2(0)=>const0,d2(1)=>const0,d2(2)=>const0,d2(3)=>const0,y1=>y2);


end structure; 
