#Python Aufgabe 1 / Modul Rechnernetze
#von Tobias Allrich

import math
import sys

def rad(a):
	b = a * (math.pi / 180)
	return b

def diff(lon1, lat1, lon2, lat2):
# Berechnet Abstand zweier Punkte (lat1,lon1)/(lat2,lon2) auf der Erdkugel
# Eingabe der Werte erfolge in Grad
# Ausgabe in km
    lon1 = rad(lon1)
    lat1 = rad(lat1)
    lon2 = rad(lon2)
    lat2 = rad(lat2)
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = math.sin(dlat/2.0)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2.0)**2
    c = 2.0 * math.asin(min(1,math.sqrt(a)))
    d = 6396.0 * c
    return d

def timesub(time1, time2):
	t1 = time1.split(":")
	t2 = time2.split(":")
	
	t_out = [int(t1[0])-int(t2[0]),int(t1[1])-int(t2[1]),int(t1[2])-int(t2[2])]
	
	if t_out[1] < 0:
		t_out[1] *= -1
		t_out[0] -= 1
	if t_out[2] < 0:
		t_out[2] *= -1
		t_out[1] -= 1
		
	return str(t_out[0]) + ":" + str(t_out[1]) + ":" + str(t_out[2])
		
	

i = 0;
strlist = []
timelist = []
weite = 0.0
long = 0.0
lat = 0.0
last_long = 0.0
last_lat = 0.0
lastpoint = []
	
#Funktioniert nur bei einem File ohne Leerzeilen
#Alles mögliche versucht um das zu umgehen, hat aber nichts geholfen. if statement gibt auch wenn line leer ist True zurück
	
file = open("data.track.txt", "r")
for line in file:
	if (line[0] != '!') and (line):
		if i == 0:
			input = line.split()
			last_lat = float(input[2]) + float(input[3])/60 + float(input[4])/3600
			last_long = float(input[5]) + float(input[6])/60 + float(input[7])/3600
			timelist.append(input[1])
			strlist.append(str(i) + ".\t" + "0.000000" + "\t" + "0.000000" + "\t" + "00:00:00" + "\t" + "00:00:00" + "\t" + "0.000000" + "\t" + input[8] + "\t" + input[2] + "\t" + input[3] + "\t" + input[4] + "\t" + input[5] + "\t" + input[6] + "\t" + input[7])
			
			i += 1
		else:
			input = line.split()
			timelist.append(input[1])		
			lat = float(input[2]) + float(input[3])/60 + float(input[4])/3600
			long = float(input[5]) + float(input[6])/60 + float(input[7])/3600
			weite += diff(last_long,last_lat,long,lat)
			t = timesub(timelist[i],timelist[i-1]).split(":")
			rt = int(t[2]) + 60*int(t[1]) + 3600*int(t[0])
			v = (diff(last_long,last_lat,long,lat)/1000)/rt
			strlist.append(str(i) + ".\t" + str(diff(last_long,last_lat,long,lat)) + "\t" + str(weite) + "\t" + str(timesub(timelist[i],timelist[i-1])) + "\t" + str(timesub(timelist[i],timelist[0])) + "\t" + str(rt) + "\t" + input[8] + "\t" + input[2] + "\t" + input[3] + "\t" + input[4] + "\t" + input[5] + "\t" + input[6] + "\t" + input[7] )
			last_long = long
			last_lat = lat
			
			i += 1

file.close()
fnew = open("track.plt","w")
for j in range(i):
	fnew.write(strlist[j]+"\n")
	
fnew.close()
