import socket
import sys

#Socket erstellen

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server = ('localhost', 8080)
print("Setting up Server on {} port {}".format(*server))
sock.bind(server)
sock.listen(1)

#Auf Verbindung warten

while True:
	print("Waiting for connection..")
	conn, client = sock.accept()
	try:
		print("Connection from", client)
		
		while True:
			data = str(conn.recv(1024),'utf-8')
			
			#Umwandeln der empfangenen Daten
			wdata = data.split('*')
						
			#Test auf syntaktische Richtigkeit
			if len(wdata) != 3:
				conn.sendall(b'Wrong input. Try <OPERATION>*<NUM1>*<NUM2>')
				break
			else:
				#Herauslesen der zwei Zahlen, wenn möglich
				try:
					a = float(wdata[1])
					b = float(wdata[2])
				except:
					conn.sendall(b'Wrong input. Try <OPERATION>*<NUM1>*<NUM2>')
					break
			
				print("Received {}".format(data))
				
				#Vergleich des Operators und anschließende Ausführung
				if wdata[0] == 'ADD':
					result = bytes('RESULT*' + str(a + b),'utf-8')
					conn.sendall(result)
					break
				if wdata[0] == 'DIF':
					result = bytes('RESULT*' + str(a - b),'utf-8')
					conn.sendall(result)
					break
				if wdata[0] == 'MUL':
					result = bytes('RESULT*' + str(a * b),'utf-8')
					conn.sendall(result)
					break
				if wdata[0] == 'DIV':
					result = bytes('RESULT*' + str(a / b),'utf-8')
					conn.sendall(result)
					break
				#Sollte keiner der vorherigen Operatoren dem Eingabeoperator entsprechen
				conn.sendall(b"Wrong operator. Try 'ADD','DIF','MUL' or 'DIV' ")
				
	finally:
		conn.close()