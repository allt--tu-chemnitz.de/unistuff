import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = sys.argv[1]

# Connect the socket to the port where the server is listening
server_address = ('localhost', int(port))
print('connecting to {} port {}'.format(*server_address))
sock.connect(server_address)

try:

    # Send data
    message = bytes(sys.argv[2], 'utf-8')
    print('sending {!r}'.format(message))
    sock.sendall(message)

    data = sock.recv(1024)
    print('received {}'.format(data))

finally:
    print('closing socket')
    sock.close()