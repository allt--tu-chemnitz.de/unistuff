cont = True
portmatrix = []

for i in range(256):
	portmatrix.append(0)

b = input('switch> Anzahl Ports wählen: ')
n = int(b)
portmatrix[255] = n+1

print(
'switch>\n'
'Eingabe bitte realisieren mit: Port Eingabeadresse Ausgabeadresse\n'
'Beispiel für 6 Ports: 5 34 25\n\n'
'Beenden mit: close, exit, x\n'
'Anzeigen der Liste mit: a'
)

while cont:
	inp = input('switch< ')
	elem = inp.split(' ')
	if len(elem) != 3:
		if len(elem) == 1:
			if elem[0] in ['exit','x','close']:
				cont = False
			if elem[0] == 'a':
				for j in range(1,n+1):
					print(j, ': ')
					for i in range(255):
						if portmatrix[i] == j:
							print(i, ' ')
					print('\n')
			if elem[0] not in ['a','x','close','exit']:
				print('switch> Ungültige Eingabe')
		else:
			print('switch> Ungültige Länge')
	else:
		if int(elem[0]) in range(n+1):
			if int(elem[1]) in range(1,255):
				if int(elem[2]) in range(1,256):
					if int(portmatrix[int(elem[2])]) == n+1:
						print('switch> Ausgabe auf allen Ports')
						if portmatrix[int(elem[1])] != int(elem[0]):
							portmatrix[int(elem[1])] = int(elem[0])
							#print('switch> Set Address ' + elem[1] + ' to Port ' + elem[0])
					elif portmatrix[int(elem[1])] != int(elem[0]):
						portmatrix[int(elem[1])] = int(elem[0])
						#print('switch> Set Address ' + elem[1] + ' to Port ' + elem[0])
						if portmatrix[int(elem[2])] in range(1,n+1):
							print('switch> Ausgabe auf Port ' + str(portmatrix[int(elem[2])]))
						elif portmatrix[int(elem[2])] == 0:
							print('switch> Ausgabe auf allen Ports')

					else:
						print('switch> Ausgabe auf Port ' + str(portmatrix[int(elem[2])]))
				else:
					print('switch> Ungültige Ausgabeadresse')
			else:
				print('switch> Ungültige Eingabeadresse')
		else:
			print('switch> Ungültiger Port')