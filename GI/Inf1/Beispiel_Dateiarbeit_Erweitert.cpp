#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

struct Auto
{
    char Marke[12];
    int PS;
};

void print(Auto* f, int len)
{
    cout<<left<<setw(12)<<setfill(' ')<<"Marke"<<setw(8)<<"PS"<<"\n";
    for(int i = 0; i < len; i++) cout<<setw(12)<<f[i].Marke<<setw(8)<<f[i].PS<<"\n";

}

int main()
{
    Auto Mietwagen[10];

    char datei[10];

    cout<<"Bitte geben Sie einen Dateinamen ein: "; //Beispieldatei: autos (ohne Dateiendung)
    cin>>datei;

    ifstream fin(datei);

    if(!fin)
    {
        cout<<"Datei nicht gefunden!";
        return 0;
    }
    else
    {
        int a;
        fin>>a;

        for(int i = 0; i < a; i++)
        {
            fin>>Mietwagen[i].Marke;
            fin>>Mietwagen[i].PS;
        }

        fin.close();

        print(Mietwagen, a);

        int neuPS;

        cout<<"Geben Sie eine PS-Zahl ein: ";
        cin>>neuPS;

        Mietwagen[a - 5].PS = neuPS;

        ofstream fout(datei);
        fout<<a<<"\n";

        for(int i = 0; i < a; i++)
        {
            fout<<Mietwagen[i].Marke<<"\n"<<Mietwagen[i].PS<<"\n";
        }

        fout.close();
    }

}
