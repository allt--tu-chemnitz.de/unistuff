#include <iostream>

using namespace std;

int pflanzen(int t)
{
    if(t == 1) return 40;
    if(t > 22 || t < 1) return 0;
    int tmp = pflanzen(t - 1);
    if(tmp < 1) return 0;
    if(tmp > 400) return (tmp * 0.62) * 1.2 - 5;
    return tmp * 1.2 - 5;

}

int main()
{
    cout<<"Jahr\tPflanzen\n";
    for(int i = 1; i < 24; i++) cout<<i<<"\t"<<pflanzen(i)<<"\n";

}
