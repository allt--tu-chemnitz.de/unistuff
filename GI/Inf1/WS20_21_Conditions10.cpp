#include <iostream>

using namespace std;

int main() {

    int datum, m, t, j, td = 0;

    cout<<"Geben Sie ein Datum ein: "; //TTMMJJJJ
    cin>>datum;

    m = (datum / 10000) % 100;
    t = datum / 1000000;
    j = datum % 10000;

    switch(m)
    {
        case 12:
            td += 30; //November
        case 11:
            td += 31; //Oktober
        case 10:
            td += 30; //September
        case 9:
            td += 31; //August
        case 8:
            td += 31; //Juli
        case 7:
            td += 30; //Juni
        case 6:
            td += 31; //Mai
        case 5:
            td += 30; //April
        case 4:
            td += 31; //M�rz
        case 3:
            if(j%400==0) td += 29;
            else
            {
                if(j%4==0 && j%100 != 0) td += 29;
                else td += 28;
            }
        case 2:
            td += 31;
        default:
            td += t;
    }

    cout<<"Der "<<t<<"."<<m<<"."<<j<<" ist der "<<td<<". Tag des Jahres";

    return 0;
}
