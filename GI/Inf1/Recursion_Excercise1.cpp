#include <iostream>

using namespace std;
/*
Aufgabe:    - jedes Jahr verdoppelt sich die Anzahl an Fischen in einem Teich
            - jedes 3. Jahr falls das Jahr davor mehr als 20 Fische im Teich gibt, -> -20
            - Tabelle als Ausgabe: "Jahr | Fische"
            - im ersten Jahr: 5 Fische
            - Nutzereingabe der Jahre
            - Nutzereingabe so lange, bis sie valide ist
            - Als Rekursion l�sen

*/

int Fische(int n)
{
    if(n <= 1) return 5;
    if(n%3 == 0 && Fische(n-1) > 20) return (Fische(n-1) - 20) * 2;
    return Fische(n-1) * 2;
}


int main()
{
    int j = 0;
    while(j < 1)
    {
        cout<<"Bitte Anzahl an Jahre eingeben (>1): ";
        cin>>j;
    }

    for(int i = 0; i < j; i++)
    {
        cout<<i+1<<" | "<<Fische(i+1)<<"\n";
    }
}
