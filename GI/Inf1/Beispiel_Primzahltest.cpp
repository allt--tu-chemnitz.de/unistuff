#include <iostream>

bool is_prim_rec(int n, int m = 2)
{
    if(n < 2) return false;
    if(n == 2) return true;
    if(2*m >= n) return (n % m != 0);
    return is_prim_rec(n, m + 1) && (n % m != 0);
}

bool is_prim_im(int n)
{
    for(int i = 2; 2 * i <= n; i++)
    {
        if(n % i == 0) return false;
    }

    return true;
}

using namespace std;

int main()
{
    cout<<"Zahl\tRek\tImp\n";
    for(int i = 2; i < 100; i++) cout<<i<<"\t"<<is_prim_rec(i)<<"\t"<<is_prim_im(i)<<"\n";
    return 0;
}
