#include <iostream>

using namespace std;

int main(void)
{
    //Variante 1
    cout<<"Technische Universität Chemnitz\ndiese vertreten durch den Rektor\nProf. Dr. Gerd Strohmeier\nStrasse der Nationen 62\n09111 Chemnitz\n";
    cout<<"\n";

    //Variante 2
    cout<<"Technische Universität Chemnitz"<<endl<<"diese vertreten durch den Rektor..."<<endl;
    cout<<"\n";

    //Variante 3
    cout<<"Technische Universität Chemnitz"<<endl;
    cout<<"diese vertreten durch den Rektor...";
    cout<<"\n";

    return 0;
}
