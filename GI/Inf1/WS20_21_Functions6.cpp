#include <iostream>

using namespace std;

float pow(float a, int b)
{
    float a1 = a;
    if(b == 0) return 1;
    for(int i = 1; i < b; i++) a *= a1;
    return a;
}

int fak(int n)
{
    int result = 1;
    while(n > 1)
    {
        result *= n;
        n--;
    }
    return result;
}

double zf(int n)
{
    double result = 0;
    for(int i = 1; i <= n; i++) result += pow(-1,i) / fak(i);
    return result;
}

int main()
{
    cout<<zf(3);
    return 0;
}
