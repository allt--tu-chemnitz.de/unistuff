#include <iostream>

using namespace std;

int main()
{
    float celsius, fahrenheit;

    cout<<"Bitte Temp. in Grad Fahrenheit angeben: ";
    cin>>fahrenheit;

    celsius = (5.0 / 9) * (fahrenheit - 32);
    cout<<"Temperatur in Grad Celsius: "<<celsius;

    return 0;
}
