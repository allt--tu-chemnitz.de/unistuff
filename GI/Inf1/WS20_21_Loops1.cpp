#include <iostream>

using namespace std;

int main()
{

    int n, sum = 0;
    cout<<"Geben Sie eine Anzahl an Zahlen >0 ein: ";
    cin>>n;

    if(n <= 0)
    {
        cout<<"Falsche Anzahl!";
        return 1;
    }

    for(int i = 0; i < n; i++) sum += 2 * (i + 1);

    cout<<"Summe der ersten "<<n<<" natuerlichen Zahlen: "<<sum;

    return 0;
}
