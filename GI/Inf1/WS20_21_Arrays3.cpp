#include <iostream>

using namespace std;

int main()
{
    int num[10] = {2, 5, 2, 3, 3, 6, 3, 7, 4, 5}, singlenum[10], l = 1;

    for(int i = 0; i < 10; i++)
    {
        bool found = false;
        if(i != 0)
        {
            for(int j = 0; j < l; j++)
            {
                if(singlenum[j] == num[i]) found = true;
            }

            if(!found)
            {
                cout<<", "<<num[i];
                singlenum[l] = num[i];
                l++;
            }
        }
        else
        {
            cout<<num[0];
            singlenum[0] = num[0];
        }


    }

    return 0;
}
