#include <iostream>
#include <iomanip>

using namespace std;

int stir(int n, int k)
{
    if(k > n) return 0;
    if(k == n || k == 1) return 1;
    return stir(n-1, k-1) + k * stir(n-1, k);
}

int main()
{
    int k;

    cout<<"Bitte geben Sie eine Menge der Partitionierungen k ein: ";
    cin>>k;

    for(int i = 1; i <= 10; i++)
    {
        cout<<left<<setfill(' ')<<"Stir("<<setw(3)<<i<<","<<setw(2)<<k<<")"<<setw(5)<<" : "<<setw(6)<<stir(i,k)<<"\n";
    }

    return 0;
}
