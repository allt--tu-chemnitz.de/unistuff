#include <iostream>
#include <cmath>

using namespace std;

struct coord
{
    int x;
    int y;
};

struct kreis
{
    coord xy;
    float r;
};

int main(void)
{
    coord p;
    kreis k;

    cout<<"Kreiskoordinaten eingeben (X,Y,r)\n";
    cin>>k.xy.x>>k.xy.y>>k.r;
    cout<<"Punktkoordinaten eingeben (X,Y)\n";
    cin>>p.x>>p.y;

    if(sqrt((p.x - k.xy.x) * (p.x - k.xy.x) + (p.y - k.xy.y) * (p.y - k.xy.y)) > k.r) cout<<"Der Punkt liegt außerhalb des Kreises";
    else cout<<"Der Punkt liegt innerhalb des Kreises";




    return 0;
}
