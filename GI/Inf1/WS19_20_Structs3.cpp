#include <iostream>
#include <cmath>

using namespace std;

struct punkt
{
    int x;
    int y;
};

struct kreis
{
    float radius;
    punkt mittelpunkt;
};

int main()
{
    kreis k;
    punkt p;


    cout<<"Bitte geben Sie den Radius des Kreises ein: ";
    cin>>k.radius;
    cout<<"Bitte geben Sie die X-Koordinate des Mittelpunkts ein: ";
    cin>>k.mittelpunkt.x;
    cout<<"Bitte geben Sie die Y-Koordinate des Mittelpunkts ein: ";
    cin>>k.mittelpunkt.y;
    cout<<"Bitte geben Sie die X-Koordinate des Punktes ein: ";
    cin>>p.x;
    cout<<"Bitte geben Sie die X-Koordinate des Punktes ein: ";
    cin>>p.y;

    double abst = sqrt(pow(p.x - k.mittelpunkt.x, 2) + pow(p.y - k.mittelpunkt.y, 2));

    cout<<"Der Punkt ("<<p.x<<";"<<p.y<<")";
    if(abst <= k.radius) cout<<" liegt im Kreis"; //Wenn der Rand noch mit zum Kreis gehört
    else cout<<" liegt nicht im Kreis";

    return 0;
}
