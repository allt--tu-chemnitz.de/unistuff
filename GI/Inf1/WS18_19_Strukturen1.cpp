#include <iostream>
#include <cmath>

using namespace std;

struct coord
{
    int x;
    int y;
};

int main(void)
{
    coord p[10];
    int tmpw = 0, tmpn = 0;
    float a[10];

    for(int i = 0; i<10; i++)
    {
        cout<<"Punkt "<<i+1<<" eingeben\n";
        cin>>p[i].x;
        cin>>p[i].y;
        cout<<"----------\n";
    }

    for(int i = 0; i<10; i++)
    {
       /* if(p[i].x < 0) p[i].x *= -1;
        if(p[i].y < 0) p[i].y *= -1; */

        a[i] = sqrt(p[i].x * p[i].x + p[i].y * p[i].y);

        if(a[i] > a[tmpw]) tmpw = i;
        if(a[i] < a[tmpn]) tmpn = i;

    }

    cout<<"Näheste Punkt: "<<p[tmpn].x<<" "<<p[tmpn].y<<"\nWeiterster Punkt: "<<p[tmpw].x<<" "<<p[tmpw].y;

    return 0;
}
