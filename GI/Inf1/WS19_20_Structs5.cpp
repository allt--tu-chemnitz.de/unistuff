#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
using namespace std;

struct Auto
{
    string Marke;
    int Kilometerstand;
    int Alter;
    int PS;
    bool Unfallwagen;
};

void bubble_sort(Auto* &a)
{
    bool sorted;
    int j = 0;
    while(!sorted)
    {
        sorted = true;
        for(int i = 0; i < (9 - j); i++)
        {
            if(a[i].Alter < a[i+1].Alter)
            {
                Auto tmp = a[i+1];
                a[i+1] = a[i];
                a[i] = tmp;
                sorted = false;
            }
        }
        j++;
    }
}

int main (void)
{
	Auto* Wagen = new Auto[10];

	ifstream einlesen ("autos.txt"); // oeffnen einer Datei zum Lesen ihrer Inhalte
	if (!einlesen) cout << "Datei konnte nicht geoeffnet werden" << endl;

	int i;

	for (i=0; i<10; i++)
	{
		einlesen >> Wagen[i].Marke; // EinfÃ¼gen der Variable die information aufnehmen soll
		einlesen >> Wagen[i].Kilometerstand;
		einlesen >> Wagen[i].Alter;
		einlesen >> Wagen[i].PS;
		einlesen >> Wagen[i].Unfallwagen;
	}
	einlesen.close();

	bubble_sort(Wagen);

	cout<<left<<setw(15)<<setfill(' ')<<"Marke"<<setw(15)<<"Kilometerstand"<<setw(8)<<"Alter"<<setw(4)<<"PS"<<setw(13)<<"Unfallwagen"<<setw(16)<<"Jahreskilometer\n";
	for(int i = 0; i < 10; i++)
    {
        cout<<left<<setw(15)<<setfill(' ')<<Wagen[i].Marke<<setw(15)<<Wagen[i].Kilometerstand<<setw(8)<<Wagen[i].Alter<<setw(4)<<Wagen[i].PS;
        if(Wagen[i].Unfallwagen) cout<<setw(13)<<"Ja";
        else cout<<setw(13)<<"Nein";

        cout<<Wagen[i].Kilometerstand / Wagen[i].Alter;
        cout<<"\n";
    }

    return 0;
}
