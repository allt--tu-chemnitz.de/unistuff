#include <iostream>

using namespace std;

int main()
{
    int a[5][3], b[5][3], c[5][3], sum[3] = {0, 0, 0};

    //A einlesen
    for(int i = 0; i < 5; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            cout<<"Zeile "<<i+1<<", Spalte "<<j+1<<" : ";
            cin>>a[i][j];

            //Spaltensumme
            sum[j] += a[i][j];

            //Quadrate in B speichern
            b[i][j] = a[i][j] * a[i][j];
            //C berechnen
            c[i][j] = a[i][j] + b[i][j];
        }
    }

    //Spaltensummen ausgabe
    cout<<"\n\nSpaltensummen:\n";
    for(int i = 0; i < 3; i++) cout<<"Summe Spalte "<<i+1<<" : "<<sum[i]<<"\n";


    //Ausgabe C
    cout<<"\n\nMatrix C:\n";
    for(int i = 0; i < 5; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            cout<<c[i][j]<<"\t";
        }
        cout<<"\n";
    }

    return 0;
}
