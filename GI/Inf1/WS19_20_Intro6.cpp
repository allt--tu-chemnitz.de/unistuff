#include <iostream>

using namespace std;

int main()
{
    int eingabe, t, m, j;

    cout<<"Bitte geben Sie eine 8-stellige ganze Zahl ein: ";
    cin>>eingabe;

    t = eingabe / 1000000;
    m = (eingabe / 10000) % 100;
    j = eingabe % 10000;

    cout<<t<<"."<<m<<"."<<j;


    return 0;
}
