#include <iostream>

using namespace std;

int main()
{
    int jahre;
    float gehalt;

    cout<<"Bitte geben Sie eine Jahreszahl ein: ";
    cin>>jahre;
    cout<<"Bitte geben Sie ein Gehalt ein: ";
    cin>>gehalt;


    switch(jahre)
    {
        case 5 ... 9:
            cout<<"3% Bonus -> "<<gehalt * 1.03;
            break;
        case 10 ... 14:
            cout<<"8% Bonus -> "<<gehalt * 1.08;
            break;
        default:
            if(jahre >= 15) cout<<"18% Bonus -> "<<gehalt * 1.18;
            else cout<<"Kein Bonus -> "<<gehalt;
    }

    return 0;
}
