#include <iostream>

using namespace std;

int bino(int n, int k)
{
    if(n == k || k == 0) return 1;
    if(n > k && k > 0) return bino(n - 1, k) + bino(n - 1, k - 1);
    else return -1;
}

int main()
{
    cout<<bino(3,3)<<" "<<bino(3,2);

}
