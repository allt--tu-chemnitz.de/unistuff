#include <iostream>

using namespace std;

int stirling(int n, int k)
{
    if(k == n || k == 1) return 1;
    if(k > n) return 0;

    return stirling(n - 1,k - 1) + k * stirling(n - 1,k);
}

int main(void)
{
    int n1,k1;
    cout<<"N eingeben, K eingeben\n";
    cin>>n1>>k1;

    cout<<stirling(n1,k1);


    return 0;
}
