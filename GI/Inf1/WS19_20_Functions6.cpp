#include <iostream>

using namespace std;

int fak(int a)
{
    for(int i = (a - 1); i > 0; i--) a *= i;
    return a;
}

double pow(double b, int e)
{
    int be = b;
    if(e == 0) return 1;
    if(e > 0)
    {
        for(int i = 1; i < e; i++) b *= be;
        return b;
    }
}

double Folge(int n)
{
    double sum = 0;
    for(int i = 1; i <= n; i++)
    {
        sum += pow(-1, i) * 1/fak(i);
    }
    return sum;
}

int main()
{

    int g = 3;
    cout<<Folge(g);
    return 0;
}
