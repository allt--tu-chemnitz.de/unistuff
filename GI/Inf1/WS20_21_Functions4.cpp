#include <iostream>

using namespace std;

int rev(int a)
{
    int result = 0;
    while(a != 0)
    {
        result *= 10;
        result += a % 10;
        a /= 10;
    }
    return result;
}

int main()
{
    cout<<rev(1234);
    return 0;
}
