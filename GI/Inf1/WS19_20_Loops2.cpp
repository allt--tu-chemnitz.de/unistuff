#include <iostream>

using namespace std;

int main()
{
    int anzahl;
    float durchschnitt, tmp, werte = 0.0, maximum;

    cout<<"Bitte geben Sie eine Anzahl an Messwerten ein: ";
    cin>>anzahl;

    if(anzahl < 1)
    {
        cout<<"Ungueltige Anzahl.";
        return 1;
    }

    for(int j = 0; j < anzahl; j++)
    {
        cout<<"Geben Sie den "<<j + 1<<". Messwert ein: ";
        cin>>tmp;
        if(j == 0) maximum = tmp;
        if(tmp > maximum) maximum = tmp;

        werte += tmp;
    }

    durchschnitt = werte / anzahl;

    cout<<"Der Durchschnitt der "<<anzahl<<" Messwerte liegt bei: "<<durchschnitt<<"\n";
    cout<<"Der groesste Messwert war: "<<maximum;


    return 0;
}
