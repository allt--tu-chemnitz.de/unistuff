#include <iostream>
#include <cmath>

using namespace std;

struct punkt
{
    int x;
    int y;
};

double sqr(double n)
{
    return n*n;
}

double abst(punkt a, punkt b)
{
    return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

int main()
{
    punkt c, d;

    c.x = 0;
    c.y = 0;
    d.x = 12;
    d.y = 0;

    cout<<"Abstand: "<<abst(c, d);

    return 0;
}
