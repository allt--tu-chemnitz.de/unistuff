#include <iostream>

using namespace std;

int main()
{
    float g = 9.81, t, s, v;

    cout<<"Bitte geben Sie eine Zeit t in Sekunden ein: ";
    cin>>t;

    v = g * t;
    s = 0.5 * g * t * t; //oder s = 0.5 * v * t

    cout<<"Zurueckgelegte Strecke im freien Fall: "<<s<<" m\nGeschwindigkeit nach t Sekunden: "<<v<<" m/s";


    return 0;
}
