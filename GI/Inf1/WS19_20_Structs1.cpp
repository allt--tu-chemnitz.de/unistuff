#include <iostream>
#include <cmath>

using namespace std;

struct punkt
{
  float x;
  float y;
};

int main()
{
    punkt a, b;
    double abstand;

    cout<<"Bitte geben Sie Punkt a X ein: ";
    cin>>a.x;
    cout<<"Bitte geben Sie Punkt a Y ein: ";
    cin>>a.y;
    cout<<"Bitte geben Sie Punkt b X ein: ";
    cin>>b.x;
    cout<<"Bitte geben Sie Punkt b Y ein: ";
    cin>>b.y;

    abstand = sqrt(pow(a.x - b.x,2) + pow(a.y - b.y, 2)); // (a.x - b.x) * (a.x - b.x) <=> pow(a.x - b.x,2)

    cout<<"Abstand zwischen den Punkten: "<<abstand;

    return 0;
}
