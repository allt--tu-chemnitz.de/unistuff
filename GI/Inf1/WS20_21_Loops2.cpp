#include <iostream>

using namespace std;

int main()
{

    int n;
    cout<<"Geben Sie eine Anzahl an Messwerten >0 ein: ";
    cin>>n;

    if(n <= 0)
    {
        cout<<"Ungueltige Anzahl!";
        return 1;
    }

    float d = 0, mess_max = 0.0;

    for(int i = 0; i < n; i++)
    {
        float tmp;
        cout<<"Geben Sie Messwert "<<i+1<<" ein: ";
        cin>>tmp;

        if(i == 0 || tmp > mess_max) mess_max = tmp;
        d += tmp;
    }

    cout<<"Der Durchschnitt ist "<<d/n<<"\nDer groesste Wert war "<<mess_max;

    return 0;
}
