#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

struct wetterdaten
{
    int datum;
    float temp;
    char wetter[50];
    float niederschlag;
};

float niederschl(wetterdaten a, wetterdaten b)
{
    if(a.niederschlag > b.niederschlag) return a.temp;
    else return b.temp;
}

int main()
{
    wetterdaten daten[29];
    float j = -273;
    int i = 0;
    char name[20];

    cin >> name;
    ifstream fin(name);

    if(!fin)
    {
        cout << "Datei nicht gefunden";
        return 1;
    }

    for(i; !fin.eof(); i++)
    {
        fin >> daten[i].datum >> daten[i].temp >> daten[i].wetter >> daten[i].niederschlag;
        cout << daten[i].datum << " " << daten[i].temp << " " << daten[i].wetter << " " << daten[i].niederschlag << "\n";
        if(!strcmp(daten[i].wetter, "Schnee") && daten[i].temp > j) j = daten[i].temp;
    }

    fin.close();

    int t1,t2;

    cout << "Hoeste Temp bei Schnee: " << j << " Grad\n";
    cout << "Tage eingeben (int): Tage 1, Tag 2\n";
    cin >> t1 >> t2;

    if(t1 > i || t2 > i || t1 < 1 || t2 < 1)
    {
        cout << "Ungueltige Werte";
        return 1;
    }

    cout << "Hoechste Temp: " << niederschl(daten[t1 - 1], daten[t2 - 1]) << " Grad";

    return 0;
}
