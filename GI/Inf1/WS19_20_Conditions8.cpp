#include <iostream>

using namespace std;

int main()
{
    int jahr;

    cout<<"Geben Sie ein Jahr ein: ";
    cin>>jahr;

    if((jahr % 4 == 0 && jahr % 100 != 0) || jahr % 400 == 0) cout<<"Das Jahr ist ein Schaltjahr";
    else cout<<"Das Jahr ist kein Schaltjahr";

    return 0;
}
