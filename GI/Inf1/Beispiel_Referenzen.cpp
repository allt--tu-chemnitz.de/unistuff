#include <iostream>

using namespace std;

int quer(int a, int &n)
{
    n = 0;
    int q = 0;

    while(a != 0)
    {
        q += a % 10;
        a /= 10;
        n++;
    }

    return q;
}

int main()
{
    int x = 5, t = 4;

    //Die Referenz beinflussen die Variablen die der Funktion �bergeben wurden

    cout<<x<<" "<<quer(123,x)<<"\n";
    cout<<t<<" "<<quer(321,t);

    return 0;
}
