#include <iostream>

using namespace std;

int main()
{
    const int MAX = 100;            // Maximaler Wert der Feldgröße
    int a[MAX], b[MAX] = {0}, u = 1, g;   // {2, 5, 2, 3, 3, 6, 3, 7, 4, 5} -> Testwerte

    cout<<"Bitte geben Sie die Feldgroesse ein: (maximal 100) ";
    cin>>g;

    if(g < 1 || g > 100) return 1;

    for(int i = 0; i < g; i++)
    {
        cout<<"Wert ";
        cout.width(3);
        cout<<i + 1;
        cout.fill(' ');
        cout<<" : ";
        cin>>a[i];
    }

    b[0] = a[0];

    for(int i = 0; i < g; i++)
    {
        bool test = true;

        for(int j = 0; j < u && test; j++)
        {
            if(a[i] == b[j])
            {
                test = false;
            }
        }

        if(test)
        {
            b[u] = a[i];
            u++;
        }
    }


    //Ausgabe
    for(int i = 0; i < u; i++)
    {
        cout<<b[i];
        if(i != (u - 1)) cout<<", ";
    }
    return 0;
}
