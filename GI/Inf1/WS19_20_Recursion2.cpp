#include <iostream>

using namespace std;

int fak(int f)
{
    if(f == 1) return 1;
    return fak(f - 1) * f;
}

int main()
{
    int zahl;

    cout<<"Bitte Zahl eingeben: ";
    cin>>zahl;

    cout<<zahl<<"! = "<<fak(zahl);

    return 0;
}
