#include <iostream>

using namespace std;

int main()
{
    int i = 0, z = 1, n, sum = 0;

    cout<<"Geben Sie ein n ein: ";
    cin>>n;

    if(n < 0)
    {
        cout<<"Ungueltiger Wert.";
        return 1;
    }

    //While Form

    while(i < n) //Hier ist z die Z�hlvariable und i gibt an, wie viel Zahlen gefunden wurden
    {
        if(z % 2 == 0)
        {
            sum += z;
            i++;
        }

        z++;
    }

    cout<<"Summe der ersten 5 natuerlichen Zahlen (while):\t"<<sum<<"\n\n";

    //For Form
    sum = 0;

    for(int j = 1; n > 0; j++)
    {
        if(j % 2 == 0)
        {
            sum += j;
            n--;
        }
    }

    cout<<"Summe der ersten 5 natuerlichen Zahlen (for):\t"<<sum<<"\n\n";

    return 0;
}
