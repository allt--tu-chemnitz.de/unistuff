#include <iostream>

using namespace std;

int main()
{
    int jahre;
    float gehalt;

    cout<<"Bitte geben Sie eine Jahreszahl ein: ";
    cin>>jahre;
    cout<<"Bitte geben Sie ein Gehalt ein: ";
    cin>>gehalt;

    if(jahre < 5 && jahre > 0)
    {
        cout<<"Kein Bonus -> "<<gehalt;
    }
    if(jahre >= 5 && jahre < 10)
    {
        cout<<"3% Bonus -> "<<gehalt * 1.03;
    }
    if(jahre >=10 && jahre < 15)
    {
        cout<<"8% Bonus -> "<<gehalt * 1.08;
    }
    if(jahre >= 15)
    {
        cout<<"18% Bonus -> "<<gehalt * 1.18;
    }


    return 0;
}
