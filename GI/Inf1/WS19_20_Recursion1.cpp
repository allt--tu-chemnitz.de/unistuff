#include <iostream>

using namespace std;

int steine(int h)
{
    if(h < 1) return 0;
    if(h == 1) return 1;
    return steine(h-1) + h;
}

int main()
{
    int hoehe = 0;

    cout<<"Bitte Hoehe eingeben: ";
    cin>>hoehe;

    cout<<"Es werden "<<steine(hoehe)<<" Steine benoetigt";

    return 0;
}
