#include <iostream>

using namespace std;

double pow(double b, int e)
{
    int be = b;
    if(e == 0) return 1;
    if(e > 0)
    {
        for(int i = 1; i < e; i++) b *= be;
        return b;
    }
    if(e < 0)
    {
        for(int i = 1; i < (e*-1); i++) b *= be;
        return 1/b;
    }
}

int main()
{
    cout<<pow(2,-2)<<"\n"; // erwartet 0.25
    cout<<pow(2,2)<<"\n";  // erwartet 4
    cout<<pow(2,0)<<"\n";  // erwartet 1
    return 0;
}
