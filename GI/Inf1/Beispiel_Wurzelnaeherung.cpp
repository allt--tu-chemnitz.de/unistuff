#include <iostream>

using namespace std;

double sqrt(double a, int i = 20)
{
    if(i == 1) return (a+1)/2;
    return 0.5*(sqrt(a, i-1) + (a / sqrt(a, i-1)));
}

int main()
{
    cout<<sqrt(144)<<"\n";
    if(sqrt(144) == 12) cout<<"Genau genug";
    else cout<<"Zu ungenau";
    return 0;
}
