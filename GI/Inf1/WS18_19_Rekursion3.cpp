#include <iostream>

using namespace std;

int adder(int *a,int i)
{

    if(i == 0) return a[i];

    return adder(a,i-1)+a[i];
}

int main(void)
{
    int l[8];

    for(int i = 0;i<8;i++)
    {
        cin>>l[i];
    }

    cout<<adder(l,7);

    return 0;
}
