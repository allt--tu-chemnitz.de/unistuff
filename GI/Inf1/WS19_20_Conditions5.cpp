#include <iostream>

using namespace std;

int main()
{
    int a, b;

    cout<<"Bitte zwei ganze Zahlen eingeben:\n";
    cin>>a>>b;

    if(a%2 == 0 || b%2 == 0) cout<<"Eine der beiden Zahlen ist durch 2 teilbar.";
    else cout<<"Keine der beiden Zahlen ist durch 2 teilbar.";

    return 0;
}
