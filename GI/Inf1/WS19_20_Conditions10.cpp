#include <iostream>

using namespace std;

int main()
{
    int datum, jahr, monat, tag = 0;
    bool sj = false;

    cout<<"Bitte geben Sie ein Datum ein (DDMMYYYY): ";
    cin>>datum;

    jahr = datum % 10000;

    if((jahr % 4 == 0 && jahr % 100 != 0) || jahr % 400 == 0) sj = true;

    monat = (datum / 10000) % 100;

    switch(monat - 1)
    {
    case 11:
        tag = tag + 30;
    case 10:
        tag = tag + 31;
    case 9:
        tag = tag + 30;
    case 8:
        tag = tag + 31;
    case 7:
        tag = tag + 31;
    case 6:
        tag = tag + 30;
    case 5:
        tag = tag + 31;
    case 4:
        tag = tag + 30;
    case 3:
        tag = tag + 31;
    case 2:
        if(sj) tag = tag + 29;
        else tag = tag + 28;
    case 1:
        tag = tag + 31;
    case 0:
        break;
    default:
        cout<<"Falscher Monat";
        return 1;
    }

    tag = tag + datum / 1000000;

    cout<<"Es ist der "<<tag<< " Tag des Jahres "<<jahr;

    return 0;
}
