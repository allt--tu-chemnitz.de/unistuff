#include <iostream>

using namespace std;

int main()
{
    float a;

    cout<<"Bitte eine Zahl eingeben: ";
    cin>>a;

    if(a < 0) a = -a;

    cout<<"|a| = "<<a;

    return 0;
}
