#include <iostream>

using namespace std;

int fak(int n)
{
    int result = 1;
    while(n > 1)
    {
        result *= n;
        n--;
    }
    return result;
}

int main()
{
    cout<<fak(4);
    return 0;
}
