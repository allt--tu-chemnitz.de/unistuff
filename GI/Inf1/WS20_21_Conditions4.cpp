#include <iostream>

using namespace std;

int main()
{
    int a, b;

    cout<<"Geben Sie im Folgenden zwei ganze Zahlen ein:\n";
    cin>>a>>b;

    if(a != 0)
    {
        if(b%a == 0) cout<<a<<" ist ein Teiler von "<<b;
        else cout<<a<<" ist kein Teiler von "<<b;
    }
    else cout<<"Division durch 0 verboten!";

    return 0;
}
