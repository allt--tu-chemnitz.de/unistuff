#include <iostream>

using namespace std;

int main()
{
    int n;
    float sum = 0.0, epsilon;

    cout<<"Geben Sie ein N ein: ";
    cin>>n;
    cout<<"Geben Sie ein Epsilon ein: ";
    cin>>epsilon;

    for(int i = 1; i <= n; i++)
    {
        if((1.0 / i - 1.0 / (i + 1)) < epsilon && i < n) break;
        sum += 1.0 / i;
    }

    cout<<"Das Ergebnis lautet: "<<sum;

    return 0;
}
