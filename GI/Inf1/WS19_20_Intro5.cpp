#include <iostream>

using namespace std;

int main()
{
    int s_e, s, m, h;

    cout<<"Bitte geben Sie eine ganze Zahl Sekunden ein: ";
    cin>>s_e;

    h = s_e / 3600;
    m = (s_e / 60) % 60;
    s = s_e % 60;

    cout<<h<<":"<<m<<":"<<s;

    return 0;
}
