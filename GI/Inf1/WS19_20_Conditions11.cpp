#include <iostream>

using namespace std;

int main()
{
    float a, b;
    char op;

    cout<<"Geben Sie Operand 1 ein: ";
    cin>>a;
    cout<<"Geben Sie einen Operator ein: ";
    cin>>op;
    cout<<"Geben Sie Operand 2 ein: ";
    cin>>b;

    switch(op)
    {
        case '+':
            cout<<"a + b = "<<a + b;
            break;
        case '-':
            cout<<"a - b = "<<a - b;
            break;
        case '*':
            cout<<"a * b = "<<a * b;
            break;
        case '/':
            cout<<"a / b = "<<a / b;
            break;
        default:
            cout<<"Ungueltiger Operator";
            return 1;
    }

    return 0;
}
