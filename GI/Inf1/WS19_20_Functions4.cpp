#include <iostream>

using namespace std;

int umkehr(int a)
{
    int u = 0;
    while(a != 0)
    {
        u *= 10;
        u += a%10;
        a /= 10;
    }

    return u;
}

int main()
{
    int b;

    cout<<"Bitte Zahl eingeben: ";
    cin>>b;
    cout<<b<<" -> "<<umkehr(b);

    return 0;
}
