#include <iostream>
#include <cmath>

using namespace std;

struct Point
{
    float x,y;
};

struct Circle
{
    Point m;
    float r;
};

int main()
{
    Circle c;
    Point p;

    cout<<"Geben Sie einen Kreismittelpunkt ein:\n";
    cin>>c.m.x>>c.m.y;
    cout<<"Geben Sie einen Radius ein: ";
    cin>>c.r;

    cout<<"Geben Sie einen Punkt ein:\n";
    cin>>p.x>>p.y;

    if(sqrt(pow(p.x - c.m.x, 2) + pow(p.y - c.m.y, 2)) <= c.r)
    {
        cout<<"Punkt p liegt im Kreis";
    }
    else cout<<"Punkt p liegt nicht im Kreis";

}
