#include <iostream>

using namespace std;

struct Point
{
    float x,y;
};

double sqr(double n)
{
    return n*n;
}

double abstand(Point a, Point b)
{
    return sqr(a.x - b.x) + sqr(a.y - b.y);
}

int main()
{
    Point a, b;
    a.x = 3;
    a.y = 0;
    b.x = 0;
    b.y = 4;
    cout<<abstand(a, b);
    return 0;
}
