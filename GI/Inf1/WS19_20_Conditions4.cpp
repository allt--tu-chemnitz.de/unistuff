#include <iostream>

using namespace std;

int main()
{
    int a, b;

    cout<<"Bitte zwei ganze Zahlen eingeben:\n";
    cin>>a>>b;

    if(a == 0)
    {
        cout<<"Division durch 0 ist ungueltig!";
        return 1;
    }

    if(b%a == 0) cout<<"a ist ein Teiler von b";
    else cout<<"a ist kein Teiler von b";

    return 0;
}
