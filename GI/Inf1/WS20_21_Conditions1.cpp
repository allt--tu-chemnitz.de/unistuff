#include <iostream>

using namespace std;

int main()
{
    float a;

    //Eingabe
    cout<<"Bitte geben Sie eine Zahl ein: ";
    cin>>a;

    //Verarbeitung
    cout<<"|a| = ";
    if(a < 0) cout<<-a;
    else cout<<a;

    return 0;
}
