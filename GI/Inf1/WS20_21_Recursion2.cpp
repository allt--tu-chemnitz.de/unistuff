#include <iostream>

using namespace std;

int fak(int f)
{
    if(f == 0) return 1;
    if(f < 0) return -fak(-f);
    return fak(f - 1) * f;
}

int main()
{
    cout<<fak(4);
    return 0;
}
