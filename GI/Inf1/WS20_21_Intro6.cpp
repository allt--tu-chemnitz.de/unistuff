#include <iostream>

using namespace std;

int main() {

    int t;

    cout<<"Geben Sie das Zeitformat TTMMJJJJ als ganze Zahl ein: ";
    cin>>t;

    cout<<t / 1000000<<"."<<t % 1000000 / 10000<<"."<<t % 10000;

    return 0;
}
