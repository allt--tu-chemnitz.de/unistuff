#include <iostream>

using namespace std;

int rehe(int n)
{
    if(n == 1) return 12;
    if(rehe(n-1) * 3 - 10 > 40) return (rehe(n-1) * 3 - 10) * 0.3; // (rehe(n-1) * 3 - 10) * (1 - 0.7)
    return rehe(n-1) * 3 - 10;
}

int main()
{
    cout<<"Jahr\tRehe\n";
    for(int i = 1; i <= 10; i++) cout<<i<<"\t"<<rehe(i)<<"\n";

}
