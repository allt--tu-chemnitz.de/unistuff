#include <iostream>

using namespace std;

int main()
{
    int eing, quer = 0, tmp;

    cout<<"Geben Sie eine natuerliche Zahl ein: ";
    cin>>eing;

    if(eing < 0)
    {
        cout<<"Ungueltiger Wert.";
        return 1;
    }

    tmp = eing;

    while(eing != 0)
    {
        quer += eing % 10;
        eing /= 10;
    }

    cout<<"Die Quersumme von "<<tmp<<" ist "<<quer;

    return 0;
}
