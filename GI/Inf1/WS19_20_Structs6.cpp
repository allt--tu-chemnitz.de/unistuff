#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

struct Arbeiter
{
    string name;
    int alter;
    double jahresgehalt;
};

int main()
{
    const int Mitarbeiterzahl = 100;
    int anz = 0;

    Arbeiter Mitarbeiter[Mitarbeiterzahl];

    while(true)
    {
        char eing;
        cout<<"\n"<<setw(100)<<setfill('#')<<"\n";
        cout<<"\n1 - alle Mitarbeiter anzeigen\n"
            <<"2 - durchschnittliches Alter ausgeben\n"
            <<"3 - Mitarbeiter einstellen\n"
            <<"4 - Gehaltserhöhung\n"
            <<"5 - Topverdiener anzeigen\n"
            <<"6 – Mitarbeiter entlassen\n"
            <<"x - Programm beenden\n\n";

            cout<<"Eingabe: ";
            cin>>eing;
            switch(eing)
            {
                case '1':
                {
                    if(anz == 0) cout<<"Keine Arbeiter eingestellt!\n";
                    cout<<left<<setw(30)<<setfill(' ')<<"Name"<<setw(7)<<"Alter"<<setw(12)<<"Jahresgehalt\n";
                    for(int i = 0; i < anz; i++)
                    {
                        cout<<left<<setw(30)<<Mitarbeiter[i].name<<setw(7)<<Mitarbeiter[i].alter<<setw(12)<<Mitarbeiter[i].jahresgehalt<<"\n";
                    }
                    break;
                }
                case '2':
                {
                    float d = 0;
                    for(int i = 0; i < anz; i++) d += Mitarbeiter[i].alter;
                    if(anz == 0) cout<<"Keine Arbeiter eingestellt!\n";
                    else
                    {
                        d /= anz;
                        cout<<"Durchschnittsalter: "<<d;
                    }
                    break;
                }
                case '3':
                {
                    if(anz == 100)
                    {
                        cout<<"Es koennen keine weiteren Arbeiter eingestellt werden!\n";
                        break;
                    }

                    cout<<"Name: ";
                    cin>>Mitarbeiter[anz].name;
                    cout<<"Alter: ";
                    cin>>Mitarbeiter[anz].alter;
                    cout<<"Jahresgehalt: ";
                    cin>>Mitarbeiter[anz].jahresgehalt;
                    anz++;
                    break;
                }
                case '4':
                {
                    for(int i = 0; i < anz; i++) Mitarbeiter[i].jahresgehalt *= 1.1;
                    cout<<"Jeder Mitarbeiter erhielt 10% mehr Jahreslohn!\n";
                    break;
                }
                case '5':
                {
                    float d = 0;
                    for(int i = 0; i < anz; i++) d += Mitarbeiter[i].jahresgehalt;
                    if(anz == 0) cout<<"Keine Mitarbeiter!";
                    else d /= anz;
                    cout<<"Durchschnittsgehalt: "<<d<<"\n";
                    cout<<left<<setw(30)<<setfill(' ')<<"Name"<<setw(7)<<"Alter"<<setw(12)<<"Jahresgehalt\n";
                    for(int i = 0; i < anz; i++)
                    {
                        if(Mitarbeiter[i].jahresgehalt >= d*1.2)
                        {
                            cout<<left<<setw(30)<<Mitarbeiter[i].name<<setw(7)<<Mitarbeiter[i].alter<<setw(12)<<Mitarbeiter[i].jahresgehalt<<"\n";
                        }
                    }
                    break;
                }
                case '6':
                {
                    bool found = false;
                    string tmp;
                    cout<<"Name eingeben: ";
                    cin>>tmp;

                    for(int i = 0; i < anz; i++)
                    {
                        if(found)
                        {
                            Mitarbeiter[i-1] = Mitarbeiter[i];
                        }

                        if(Mitarbeiter[i].name == tmp) found = true;
                    }
                    if(found)
                    {
                        anz--;
                        cout<<"Mitarbeiter "<<tmp<<" wurde entlassen!\n";
                    }
                    else cout<<"Kein Mitarbeiter mit diesem Name gefunden!\n";
                    break;
                }
                case 'x':
                    return 0;
                default:
                    cout<<"Falsche Eingabe!\n";
            }
    }

    return 0;
}
