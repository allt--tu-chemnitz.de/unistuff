#include <iostream>

using namespace std;

int main()
{
    int sum_max, sum, a[1000], n, io_max = 0, iu_max = 0;

    cout<<"Geben Sie die Anzahl der einzugebenden Zahlen ein: (1-1000) ";
    cin>>n;

    if(n < 1 || n > 1000) return 1;

    for(int i = 0; i < n; i++)
    {
        cout<<"Wert "<<i+1<<": ";
        cin>>a[i];
    }

    sum_max = a[0];

    for(int i = 0; i < n; i++)
    {
        sum = 0;

        for(int j = i; j < n; j++)
        {
            sum += a[j];
            if(sum > sum_max)
            {
                iu_max = i;
                io_max = j;
                sum_max = sum;
            }
        }
    }

    cout<<"Die maximale Summe ist "<<sum_max<<" von Index "<<iu_max<<" bis Index "<<io_max;

    return 0;
}
