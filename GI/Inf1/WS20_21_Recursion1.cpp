#include <iostream>

using namespace std;

int lego(int h)
{
    if(h <= 0) return 0;
    return lego(h - 1) + h;
}

int main()
{
    cout<<lego(4);
    return 0;
}
