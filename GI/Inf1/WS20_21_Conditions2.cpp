#include <iostream>

using namespace std;

int main()
{
    char t;

    cout<<"Sind Sie krank? (y/n) : ";
    cin>>t;
    if(t == 'y') cout<<"Gute Besserung!";
    if(t == 'n') cout<<"Dann ist ja gut!";
    if(t != 'y' && t != 'n') cout<<"Keine gueltige Eingabe!";

    return 0;
}
