#include <iostream>

using namespace std;

int main()
{
    int jahr, a, b, c, d, e, k, p, q, m, n, o;

    cout<<"Bitte geben Sie ein Jahr ein: ";
    cin>>jahr;

    a = jahr % 19;
    b = jahr % 4;
    c = jahr % 7;

    k = jahr / 100;
    p = (8 * k + 13) / 25;
    q = k /4;

    m = (15 + k - p - q) % 30;
    d = (19 * a + m) % 30;
    n = (4 + k - q) % 7;

    e = (2 * b + 4 * c + 6 * d + n) % 7;
    o = 22 + d + e;

    cout<<"Oster im Jahr "<<jahr<<" ist der ";
    if(o <= 31) cout<<o<<". Maerz";
    else cout<<o - 31<<". April";


    return 0;
}
