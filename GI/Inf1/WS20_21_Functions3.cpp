#include <iostream>

using namespace std;

float pow(float a, int b)
{
    float a1 = a;
    if(b < 0) return 0;
    if(b == 0) return 1;
    for(int i = 1; i < b; i++) a *= a1;
    return a;
}

int main()
{
    cout<<pow(2.5,2);
    return 0;
}
