#include <iostream>

using namespace std;

float pow(float a, int b)
{
    float a1 = a;
    if(b == 0) return 1;
    for(int i = 1; i < b; i++) a *= a1;
    return a;
}

int fak(int n)
{
    int result = 1;
    while(n > 1)
    {
        result *= n;
        n--;
    }
    return result;
}

double exp(float x, int s=20)
{
    double result = 0;
    for(int i = 0; i < s; i++) result += pow(x,i) / fak(i);
    return result;
}

int main()
{
    cout<<exp(1);
    return 0;
}
