#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main()
{
    //Bei der Dateiarbeit wird mit C-Strings gearbeitet
    char name[10];

    cout<<"Bitte geben Sie den Name der Datei ein, in die Daten gespeichert werden sollen: ";
    cin>>name; //Das Dateiformat muss mit im Name angegeben werden (Bsp: Datei.txt)

    //Schreiben
    ofstream fout(name);

    fout<<100<<"\n";    //Die neuen Zeilen m�ssen f�r das Einlesen erzeugt werden

    for(int i = 0; i < 100; i++)
    {
        fout<<i<<"\n";
    }

    fout.close(); //Nicht vergessen den Stream nach Abarbeitung zu schlie�en!

    //Lesen
    ifstream fin(name);

    //Beim Einlesen muss man immer �berpr�fen, ob es die Datei �berhaupt gibt:
    if(!fin)
    {
        cout<<"Datei existiert nicht!";
        return 0; //Falls nichts anderes verlangt wird
    }
    else
    {
        int j;
        fin>>j; //Meist steht am Anfang beim Einlesen wie viele Werte es gibt, aber VORSICHT: NICHT immer!!

        for(int i = 0; i < j; i++)
        {
            int a;
            fin>>a;     //Einlesen funktioniert Zeilenweise
            cout<<left<<setw(12)<<setfill(' ')<<"Wert "<<i<<": "<<a<<"\n";  //M�gliche Formatierungen beachten!
        }

        fin.close(); //Auch nach dem Einlesen nicht vergessen den Stream zu schlie�en!
    }


    return 0;
}
