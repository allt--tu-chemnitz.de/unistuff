#include <iostream>

using namespace std;

int breite(int b)
{
    if(b==1) return 0;

    return 2*breite(b-1)+1;
}

void lineal(int d,int n)
{
    if(d > 0)
    {
        cout.fill(' ');
        cout.width(breite(d)+1);

        for(int i = 1;i<=n;i++)
        {
            cout<<"|";
            cout.fill(' ');
            cout.width(breite(d)+1);
        }
        cout<<"\n";

        lineal(d - 1,2*n+1);

    }



}

int main(void)
{
    int d;
    cin>>d;

    lineal(d,1);

    return 0;
}
