#include <iostream>

using namespace std;

int main() {

    const float pi = 3.1415;
    float r;

    cout<<"Geben Sie einen Radius ein: ";
    cin>>r;
    cout<<"Umfang: "<<2*pi*r<<" | Flaeche: "<<r*r*pi;

    return 0;

}
