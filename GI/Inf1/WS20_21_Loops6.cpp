#include <iostream>

using namespace std;

int main()
{
    int a, b = 0, z = 1, tmp;

    cout<<"Geben Sie eine natuerliche Zahl ein: ";
    cin>>a;

    if(a < 0)
    {
        cout<<"Ungueltige Eingabe!";
        return 1;
    }

    tmp = a;

    while(a != 0)
    {
        b += z * (a % 2);
        a /= 2;
        z *= 10;
    }

    cout<<"Binaerzahl zu "<<tmp<<" = "<<b;

    return 0;
}
