#include <iostream>

using namespace std;

int main() {

    int j;

    cout<<"Geben Sie ein Jahr ein: ";
    cin>>j;

    cout<<"Das Jahr "<<j;

    if(j%4==0)
    {
        if(j%100 == 0 && j%400 != 0) cout<<" ist kein Schaltjahr";
        else
        {
            if(j%400 == 0 || j%100 != 0) cout<<" ist ein Schaltjahr";
        }
    }
    else cout<<" ist kein Schaltjahr";

    return 0;
}
