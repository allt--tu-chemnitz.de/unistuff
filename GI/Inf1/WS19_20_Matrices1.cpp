#include <iostream>

using namespace std;

int main()
{   // Richtig :
    int sudoku[9][9] = {{7,6,5,4,1,2,9,3,8}, {1,8,2,9,5,3,4,7,6}, {3,9,4,7,6,8,1,5,2}, {8,1,6,3,2,9,5,4,7}, {9,4,3,8,7,5,6,2,1}, {2,5,7,6,4,1,8,9,3}, {4,2,9,1,8,7,3,6,5}, {5,3,8,2,9,6,7,1,4}, {6,7,1,5,3,4,2,8,9}};
    // Falsch :
    //int sudoku[9][9] = {{7,6,5,4,1,2,9,3,8}, {1,8,2,9,5,3,4,8,6}, {3,9,4,7,6,8,1,5,2}, {8,1,6,3,2,9,5,4,7}, {9,4,3,8,7,5,6,2,1}, {2,5,7,6,4,1,8,9,3}, {4,2,9,1,8,2,3,6,5}, {5,3,8,2,9,6,7,1,4}, {6,7,1,5,3,4,2,8,9}};
    bool test = true;
    int ze[9] = {0}, sp[9] = {0}, ind;

    //Eingabe
    /*
    for(int i = 0; i < 9; i++)
    {
        for(int j = 0; j < 9; j++)
        {
           cout<<"Zeile "<<i<<" , Spalte "<<j<<" : ";
           cin>>sudoku[i][j];
           if(sudoku[i][j] < 1 || sudoku[i][j] > 9) return 1;
        }
    }
*/
    for(int i = 0; i < 9; i++)
    {
        for(int j = 0; j < 9; j++)
        {
            ze[j] = sudoku[i][j];
        }

        ind = 0;

        for(int z = 1; z <= 9; z++)
        {
            for(int j = 0; j < 9; j++)
            {
                if(ze[j] == z && ind < z)
                {
                    ind++;
                }
            }
        }

        if(ind != 9)
        {
            cout<<"Sudoku falsch!";
            return 0;
        }
    }

    for(int i = 0; i < 9; i++)
    {
        for(int j = 0; j < 9; j++)
        {
            sp[j] = sudoku[j][i];
        }

        ind = 0;

        for(int z = 1; z <= 9; z++)
        {
            for(int j = 0; j < 9; j++)
            {
                if(sp[j] == z && ind < z)
                {
                    ind++;
                }
            }
        }

        if(ind != 9)
        {
            cout<<"Sudoku falsch!";
            return 0;
        }
    }

    cout<<"Sudoku richtig.";

    return 0;
}
