#include <iostream>
#include <cmath>


using namespace std;

struct punkt
{
  int x;
  int y;
};

int main()
{
    punkt punkte[10];
    double min_abst, max_abst = 0;
    int min_p = 0, max_p = 0;

    for(int i = 0; i < 10; i++)
    {
        cout<<"Geben Sie X"<<i+1<<" ein: ";
        cin>>punkte[i].x;
        cout<<"Geben Sie Y"<<i+1<<" ein: ";
        cin>>punkte[i].y;

        if(!i)
        {
            min_abst = sqrt(pow(punkte[0].x,2) + pow(punkte[0].y,2));
        }

        double abst = sqrt(pow(punkte[i].x,2) + pow(punkte[i].y,2));
        if(abst < min_abst)
        {
            min_p = i;
            min_abst = abst;
        }
        if(abst > max_abst)
        {
            max_p = i;
            max_abst = abst;
        }
    }

    cout<<"Der Punkt mit dem geringsten Abstand ist Punkt "<<min_p+1<<" mit einem Abstand von "<<min_abst<<"\n";
    cout<<"Der Punkt mit dem groessten Abstand ist Punkt "<<max_p+1<<" mit einem Abstand von "<<max_abst<<"\n";

    return 0;
}
