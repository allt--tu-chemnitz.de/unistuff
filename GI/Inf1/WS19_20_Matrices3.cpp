#include <iostream>

using namespace std;

int main()
{
    int rm[6][15] = {0};

    for(int i = 0; i < 6; i++)
    {
        cin>>rm[i][0];
    }

    for(int i = 0; i < 6; i++)
    {
        for(int j = 1; j < 15; j++)
        {
            if(i == 0) rm[i][j] = rm[i][j-1] + rm[i+1][j];
            else
            {
                if(i == 5) rm[i][j] = rm[i][j-1] + rm[i-1][j-1];
                else
                {
                    rm[i][j] = rm[i-1][j-1] - rm[i][j-1] - rm[i+1][j-1];
                }
            }
        }
    }

    for(int i = 0; i < 6; i++)
    {
        for(int j = 1; j < 15; j++)
        {
            cout<<rm[i][j]<<"\t";
        }
        cout<<"\n";
    }

    return 0;
}
