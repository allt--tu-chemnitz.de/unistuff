#include <iostream>

using namespace std;

struct pix
{
    int r;
    int g;
    int b;
};

int main()
{
    pix bild[10][10];

    for(int i = 0; i < 10; i++)             //Testwerte
    {
        for(int j = 0; j < 10; j++)
        {
            if(i == j || i == j-1 || i-1 == j)
            {
                bild[i][j].r = 255;
                bild[i][j].g = 255;
                bild[i][j].b = 255;
            }
            else
            {
                bild[i][j].r = 0;
                bild[i][j].g = 0;
                bild[i][j].b = 0;
            }
        }
    }

    int s = 0, w = 0;

    for(int i = 0; i < 10; i++)
    {
        for(int j = 0; j < 10; j++)
        {
            if(bild[i][j].r == 255 && bild[i][j].g == 255 && bild[i][j].b == 255) w++;
            if(bild[i][j].r == 0 && bild[i][j].g == 0 && bild[i][j].b == 0) s++;
        }
    }

    cout<<"Das Testbild hat "<<s<<" Schwarze und "<<w<<" Weisse Pixel";

    return 0;
}
