#include <iostream>

using namespace std;

char to_upper(char b)
{
    switch(b)
    {
        case 'A':
        case 'a':
            return 'A';
        case 'B':
        case 'b':
            return 'B';
        case 'C':
        case 'c':
            return 'C';
        case 'D':
        case 'd':
            return 'D';
        case 'E':
        case 'e':
            return 'E';
        case 'F':
        case 'f':
            return 'F';
        case 'G':
        case 'g':
            return 'G';
        case 'H':
        case 'h':
            return 'H';
        case 'I':
        case 'i':
            return 'I';
        case 'J':
        case 'j':
            return 'J';
        case 'K':
        case 'k':
            return 'K';
        case 'L':
        case 'l':
            return 'L';
        case 'M':
        case 'm':
            return 'M';
        case 'N':
        case 'n':
            return 'N';
        case 'O':
        case 'o':
            return 'O';
        case 'P':
        case 'p':
            return 'P';
        case 'Q':
        case 'q':
            return 'Q';
        case 'R':
        case 'r':
            return 'R';
        case 'S':
        case 's':
            return 'S';
        case 'T':
        case 't':
            return 'T';
        case 'T':
        case 'u':
            return 'U';
        case 'V':
        case 'v':
            return 'V';
        case 'W':
        case 'w':
            return 'W';
        case 'X':
        case 'x':
            return 'X';
        case 'Y':
        case 'y':
            return 'Y';
        case 'Z':
        case 'z':
            return 'Z';
        default:
            return '#';
    }
}

int main()
{
    int wort_l = 0;
    char wort[31]; // \0 -> Abschlusszeichen

    cout<<"Bitte geben Sie ein Wort ein: (max. 30 Zeichen) ";
    cin>>wort;

    while(wort[wort_l] != '\0')
    {
        wort[wort_l] = to_upper(wort[wort_l]);
        if(wort[wort_l] == '#')
        {
            cout<<"Wort nicht vollstaendig aus zulaessigen Buchstaben!";
            return 1;
        }
        wort_l++;
    }

    if(wort_l < 1) return 1;

    for(int i = 0; i < wort_l / 2; i++)
    {
        if(wort[i] != wort[wort_l - i - 1])
        {
            cout<<"Kein Palindrom.";
            return 0;
        }
    }


    cout<<"Palindrom.";
    return 0;
}
