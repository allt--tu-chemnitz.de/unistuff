#include <iostream>

using namespace std;

int main()
{
    int ein;

    cout<<"Bitte geben Sie eine Uhrzeit ein: ";
    cin>>ein;

    cout<<"Es ist ";

    switch(ein%100)
    {
        case 15:
            cout<<"viertel ";
            if(ein/100 >= 12) cout<<ein/100 - 11;
            else cout<<ein/100 + 1;
            break;
        case 30:
            cout<<"halb ";
            if(ein/100 >= 12) cout<<ein/100 - 11;
            else cout<<ein/100 + 1;
            break;
        case 45:
            cout<<"dreiviertel ";
            if(ein/100 >= 12) cout<<ein/100 - 11;
            else cout<<ein/100 + 1;
            break;
        case 0:
            cout<<"um ";
            if(ein/100 > 12) cout<<ein/100 - 12;
            else cout<<ein/100;
            break;
        default:
            cout<<"Keine gueltige Minutenzahl";
    }

    return 0;
}
