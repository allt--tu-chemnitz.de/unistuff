#include <iostream>
#include <iomanip>

using namespace std;

int rehe(int j)
{
    if(j < 1) return 0;
    if(j == 1) return 12;

    if(rehe(j - 1) > 40) return (double)rehe(j - 1) * 0.9 - 10;

    return rehe(j - 1) * 3 - 10;
}

int main()
{
    cout<<left<<setw(7)<<setfill(' ')<<"Rehe |"<<setw(5)<<"Jahr"<<"\n";
    cout<<"-----+------\n";

    for(int i = 10; i > 0; i--)
    {
        cout<<setw(5)<<setfill(' ')<<rehe(i)<<"| "<<setw(5)<<i<<"\n";
        cout<<"-----+------\n";
    }

    return 0;
}
