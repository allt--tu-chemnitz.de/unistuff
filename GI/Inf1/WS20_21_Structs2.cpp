#include <iostream>
#include <cmath>

using namespace std;

struct Point
{
    float x, y;
};

int main()
{
    Point p[2];
    int min_i = 0, max_i = 0;

    for(int i = 0; i < 2; i++)
    {
        cout<<"Geben Sie p.x ein: ";
        cin>>p[i].x;
        cout<<"Geben Sie p.y ein: ";
        cin>>p[i].y;

        if(sqrt(pow(p[i].x,2)) + pow(p[i].y, 2) > sqrt(pow(p[max_i].x,2)) + pow(p[max_i].y, 2))
        {
            max_i = i;
        }
        if(sqrt(pow(p[i].x,2)) + pow(p[i].y, 2) < sqrt(pow(p[min_i].x,2)) + pow(p[min_i].y, 2))
        {
            min_i = i;
        }
    }

    cout<<"Am weitesten entfernt liegt Punkt "<<max_i+1<<"\n"<<"Am n�chsten dran liegt Punkt "<<min_i+1;
}
