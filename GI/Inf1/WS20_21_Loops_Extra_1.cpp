#include <iostream>

using namespace std;

int main()
{
    int i = 0;
    float tmp = -1;

    float d = 0, mess_max = 0.0;

    while(tmp != 0)
    {
        cout<<"Geben Sie Messwert "<<i+1<<" ein: ";
        cin>>tmp;

        if(tmp != 0)
        {
            if(i == 0 || tmp > mess_max) mess_max = tmp;
            d += tmp;
            i++;
        }
    }

    if(i != 0) cout<<"Der Durchschnitt ist "<<d/i<<"\nDer groesste Wert war "<<mess_max;
    else cout<<"Keine Messwerte!";

}
