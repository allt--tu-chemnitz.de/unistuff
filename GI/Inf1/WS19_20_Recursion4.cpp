#include <iostream>

using namespace std;

int bino(int n, int k)
{
    if(n == k || k == 0) return 1;
    return bino(n-1, k) + bino(n - 1, k - 1);
}

int main()
{
    int n, k;

    cout<<"Geben Sie n und k ein:\n";
    cin>>n>>k;

    cout<<"Bino(n,k) = "<<bino(n, k);

    return 0;
}
