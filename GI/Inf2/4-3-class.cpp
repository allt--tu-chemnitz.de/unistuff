#include <iostream>

using namespace std;

class colour
{
private:
    int rgb[3];
    char hex[6];
    char int_to_char(int x);
    void rgb_to_hex();

public:
  colour(int r, int g, int b)  ;
  colour generate_complementary();
  colour& operator = (const colour&);
  friend ostream& operator << (ostream&, const colour&);
};

colour::colour(int r, int g, int b)
{
    if(r < 0 || r > 255) this->rgb[0] = 0;
    else this->rgb[0] = r;
    if(g < 0 || g > 255) this->rgb[1] = 0;
    else this->rgb[1] = g;
    if(b < 0 || b > 255) this->rgb[2] = 0;
    else this->rgb[2] = b;

    this->rgb_to_hex();
}

char colour::int_to_char(int x)
{
    switch(x)
    {
        case 15: return 'f'; case 14: return 'e'; case 13: return 'd';
        case 12: return 'c'; case 11: return 'b'; case 10: return 'a';
        case 9: return '9'; case 8: return '8'; case 7: return '7';
        case 6: return '6'; case 5: return '5'; case 4: return '4';
        case 3: return '3'; case 2: return '2'; case 1: return '1';
        case 0: return '0';
    }
}

void colour::rgb_to_hex()
{
    for(int i = 0; i < 3; i++)
    {
        this->hex[2*i] = int_to_char(this->rgb[i] / 16);
        this->hex[2*i+1] = int_to_char(this->rgb[i] % 16);
    }
}

colour colour::generate_complementary()
{
    colour comp(255 - this->rgb[0],255 - this->rgb[1],255 - this->rgb[2]);
    return comp;
}

colour& colour::operator=(const colour& c)
{
    for(int i = 0; i < 3; i++)
    {
        this->rgb[i] = c.rgb[i];
        this->hex[2*i] = c.hex[2*i];
        this->hex[2*i+1] = c.hex[2*i+1];
    }

    return *this;
}

ostream& operator << (ostream& os, const colour& c)
{
    os << "rgb: " ;

    for(int i = 0; i < 3; i++)
    {
        os.width(5);
        os.fill(' ');
        os << c.rgb[i];
        if(i < 2) os  << ", ";
    }



    os << " | hex #";

    for(int i = 0; i < 6; i++)
    {
        os << c.hex[i];
    }

    os << "\n";

    return os;
}

int main()
{
    colour a(255,255,255);

    colour b = a.generate_complementary();

    cout << a << b;

    return 0;
}
