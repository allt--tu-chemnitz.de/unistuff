#include <iostream>
#include <fstream>

using namespace std;

class Uhr
{
private:
    int stunde;
    int minute;
    int sekunde;
    bool check_h(int wert);
    bool check_ms(int wert);
public:
    Uhr(int z);
    void print();
    bool safeto(char* file);
};

bool Uhr::check_h(int wert)
{
    if(wert > 24 || wert < 0) return false;
    return true;
}

bool Uhr::check_ms(int wert)
{
    int m = wert / 100;
    int s = wert % 100;

    if((m > 59) || (s > 59) || (m < 0) || (s < 0)) return false;
    return true;
}

Uhr::Uhr(int z)
{
    if(check_h(z/10000) && check_ms(z%10000))
    {
        if((z / 10000) == 24) this->stunde = 0;
        else this->stunde = z / 10000;
        this->minute = (z % 10000) / 100;
        this->sekunde = z % 100;
    }
    else
    {
        this->stunde = -1;
        this->minute = -1;
        this->sekunde = -1;
    }
}

void Uhr::print()
{
    int h = this->stunde;
    int m = this->minute;
    int s = this->sekunde;

    if(h < 10 && h >= 0) cout << "0" << h << ":";
    else cout << h << ":";

    if(m < 10 && m >= 0) cout << "0" << m << ":";
    else cout << m << ":";

    if(s < 10 && s >= 0) cout << "0" << s;
    else cout << s;
}

bool Uhr::safeto(char* file)
{
    ofstream fopen(file);
    if(!fopen)
    {
        return false;
    }
    fopen << this->stunde << ":" << this->minute << ":" << this->sekunde;
    fopen.close();
    return true;
}

int main()
{
    int zeit = 0;
    char file[20];
    cin >> zeit;
    cin >> file;
    Uhr mittag(zeit);
    mittag.print();
    if(!mittag.safeto(file)) cout << "Couldn't safe to file";
    return 0;
}
