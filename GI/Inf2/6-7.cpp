#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

class matrix
{
    private:
        int zeilen, spalten;
        int** mtrx;
    public:
        matrix();
        matrix(int z, int s);
        ~matrix();
        bool read_from_file(char* file);
        void fill_random();
        void print();
        int neighbors_dif_three();
        matrix operator+(matrix const &A);
        matrix& operator=(matrix const &A);
};

matrix::matrix()
{
    mtrx = NULL;
    zeilen = 0;
    spalten = 0;
}

matrix::matrix(int z, int s)
{
    mtrx = new int*[z];
    for(int i = 0; i < z; i++)
    {
        mtrx[i] = new int[s];
        for(int j = 0; j < s; j++)
        {
            mtrx[i][j] = 0;
        }
    }

    zeilen = z;
    spalten = s;
}

matrix::~matrix()
{
    for(int i = 0; i < zeilen; i++) delete[] mtrx[i];
    delete[] mtrx;
    mtrx = NULL;
    zeilen = 0;
    spalten = 0;

}

bool matrix::read_from_file(char* file)
{
    fstream fout(file);

    if(!fout) return false;
    else
    {
        int z = 0;
        int s = 0;

        fout >> z >> s;

        if(z != zeilen || s != spalten)
        {
            for(int i = 0; i < zeilen; i++) delete[] mtrx[i];
            delete[] mtrx;
            mtrx = NULL;
            zeilen = 0;
            spalten = 0;
            matrix(z,s); // ? geht das ?
        }

        //for(int i = 0)
    }
}

void matrix::fill_random()
{
   for(int i = 0; i < zeilen; i++)
   {
       for(int j = 0; j < spalten; j++)
       {
           mtrx[i][j] = rand() % 11;
       }
   }
}

void matrix::print()
{
   for(int i = 0; i < zeilen; i++)
   {
        for(int j = 0; j < spalten; j++)
        {
            cout.width(3);
            cout.fill(' ');
            cout << mtrx[i][j];
        }
        cout << "\n";
   }
}

int matrix::neighbors_dif_three()
{

}

matrix matrix::operator+(matrix const &A)
{

}

int main()
{
    matrix T(5,5);
    T.fill_random();
    T.print();
    return 0;
}
