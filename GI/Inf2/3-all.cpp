#include <iostream>

using namespace std;

struct liste
{
    int data;
    liste *pNext;
};

//3-1

void print(liste* head)
{
    if(!head)
    {
        cout << "Leere liste";
        return;
    }
    while(head)
    {
        cout << head->data << "\n";
        head = head->pNext;
    }
}

//3-2

int sizes(liste* head)
{
    int i;
    for(i = 0; head; i++)
    {
        head = head->pNext;
    };
    return i;
}

//3-3

int summe(liste* head)
{
    int sum = 0;
    while(head)
    {
        sum += head->data;
        head = head->pNext;
    }
    return sum;
}

//3-4

float anzneg(liste* head)
{
    if(!head) return 0;
    int anz = 0;
    liste *tmp = head;
    while(head)
    {
        if(head->data < 0) anz++;
        head = head->pNext;
    }
    return float(anz)/sizes(tmp);
}

//3-5

void insert_front(liste* &head, int d)
{
    liste* tmp = new liste;
    tmp->pNext = head;
    tmp->data = d;
    head = tmp;
}

//3-6

void insert_back(liste* &head, int d)
{
    liste* tmp = new liste;
    if(!head)
    {
        insert_front(head, d);
        return;
    }

    while(head->pNext)
    {
        head = head->pNext;
    }

    head->pNext = tmp;
    tmp->data = d;
    tmp->pNext = 0;
}

//3-7

void insert_at(liste* &head, int pos, int d)
{
    if(!pos)
    {
        insert_front(head, d);
        return;
    }
    if(pos > sizes(head))
    {
        insert_back(head, d);
        return;
    }
    else
    {
        liste* tmp = new liste;
        int i = 1;
        while(head && i != pos)
        {
            head = head->pNext;
            i++;
        }

    }
}

//3-8 nicht ganz, da der Sachverhalt quatsch ist

void sort_list(liste* head)
{
    liste* pAnkor = head;

    int p = 0;

    //Und jetzt Bubble-Sort:
    while(p - sizes(pAnkor))
    {
        head = pAnkor;
        while(head->pNext)
        {
            if(head->data > head->pNext->data)
            {
                int pSort_data = head->data;
                head->data = head->pNext->data;
                head->pNext->data = pSort_data;
            }
            head = head->pNext;
        }
        p++;
    }
}

//3-9

void remove_front(liste* &head)
{
    if(!head) return;
    liste* pDel = head;
    head = pDel->pNext;
    delete pDel;
}

//3-10

void remove_back(liste* head)
{
    if(!head) return;
    while(head->pNext->pNext)
    {
        head = head->pNext;
    }

    liste* pDel = head;

    pDel->pNext = 0;
    pDel = head->pNext;
    delete pDel;
}

//3-11

void remove_list(liste* &head)
{
    if(!head) return;
    while(head->pNext)
    {
        remove_front(head);
    }
}


int main()
{
    liste* ANKER = 0;

    insert_back(ANKER, 17);
    insert_front(ANKER, -42);
    insert_front(ANKER, 4);
    insert_front(ANKER, 86);
    sort_list(ANKER);
  //  remove_front(ANKER);
  //  remove_list(ANKER);
    print(ANKER);
    cout << "Size: " << sizes(ANKER) << "\nSum: " << summe(ANKER) << "\nNeg. Elements: " << anzneg(ANKER)*100 <<"%";

    return 0;
}
