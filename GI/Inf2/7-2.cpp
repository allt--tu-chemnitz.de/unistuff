#include <iostream>
#include <string>
#include <fstream>

using namespace std;

struct biene
{
    int id;
    int nektar;
    string aufgabe;
    biene* next;
};

class bienenvolk
{
    private:
        string queen, *tasks;
        float honey_am;
        int tasks_am;
        biene* head, *tail;

    public:
        bienenvolk(string n);
        bienenvolk(string n, int tam, string* t); //tam -> task ammount
        float how_much_honey();
        bool insert_back(int i, int n, string a); //i = ID
        bool read_from_file(char* d); //d = Dateiname
        bool write_to_file(char* d); //d = Dateiname
        bool operator==(bienenvolk &A);
        int operator+(bienenvolk &A);
       // void friend operator<<(int blub); //Überdenken bitte
};

bienenvolk::bienenvolk(string n)
{
    queen = n;
    tasks = NULL;
    tasks_am = 0;
    head = tail = NULL;
    honey_am = 0;
}

bienenvolk::bienenvolk(string n, int tam, string* t)
{
    queen = n;
    tasks = t;
    tasks_am = tam;
    head = tail = NULL;
    honey_am = 0;
}

float bienenvolk::how_much_honey()
{
    if(!head)
    {
        return 0;
    }

    biene* tmp = head;
    int sum = 0;

    while(tmp)
    {
        if(tmp->aufgabe == "Nektarsammler") sum += tmp->nektar;
        tmp = tmp->next;
    }

    sum /= 1000000;

    return (sum / 3); //Ausgabe in kg
}

bool bienenvolk::insert_back(int i, int n, string a)
{
    if(!head)
    {
        head = new biene;
        head->id = i;
        head->nektar = n;
        head->aufgabe = a;
        head->next = NULL;
        tail = head;
        return true;
    }

    biene* tmp = head;

    while(tmp)
    {
        if(tmp->id == i) return false;
        tmp = tmp->next;
    }

    tmp = new biene;
    tail->next = tmp;
    tmp->id = i;
    tmp->nektar = n;
    tmp->aufgabe = a;
    tmp->next = NULL;
    tail = tmp;
    return true;
}

bool bienenvolk::read_from_file(char* d)
{
    ifstream fin(d);

    if(!fin) return false;
    else
    {
        fin >> tasks_am;

        string* tmp = new string[tasks_am];

        for(int i = 0; i < tasks_am; i++)
        {
            fin >> tmp[i];
        }

        tasks = tmp;
        int anzbienen = 0;

        while(!fin.eof())
        {
            if(!fin.eof()) break;

            biene* tbiene;

            fin >> tbiene->id;
            fin >> tbiene->nektar;
            fin >> tbiene->aufgabe;

            if(this->insert_back(tbiene->id, tbiene->nektar, tbiene->aufgabe)) anzbienen++;
        }

        cout << "Es wurden insgesamt " << anzbienen << "hinzugefügt\n";
        fin.close();
    }
}

bool bienenvolk::operator==(bienenvolk &A)
{
    if(this->queen == A.queen) return true;
    return false;
}

int main()
{
    bienenvolk A("Hilde");
    bienenvolk B("Olga");

    if(A == B) cout << "Gleich";
    else cout << "Unterschiedlich";

    return 0;
}
