#include <iostream>

using namespace std;

class stapel
{
    private:
        char* sp;
        char* oben;
        int size;
        int anzelem;
    public:
        stapel(int i);
        bool push(char c);
        char pop();
        bool swap(int p, char r);

};

stapel::stapel(int i)
{
    sp = new char[i];
    oben = NULL;
    size = i;
    anzelem = 0;
}

bool stapel::push(char c)
{
    if(oben != (sp + size))
    {
        if(!oben)
        {
            oben = sp;
        }
        else oben++;

        *oben = c;
        anzelem++;

        cout << "Groesse: " << anzelem << " Oberstes Element: " << *oben << "\n";

        return true;
    }
    else return false;

}

char stapel::pop()
{
    if(oben)
    {
        char tmp = *oben;
        if(oben == sp)
        {
            oben = NULL;
        }
        else oben--;
        anzelem--;
        return tmp;
    }
    else
    {
        cout << "Stapel leer\n";
        return ' ';
    }
}

bool stapel::swap(int p, char r)
{
    char* tmp = sp;

    if(r != 'o' && r != 'u') return false;
    if(p > anzelem || p > size) return false;

    //cout << sp[0] << " " << sp[1] << " " << sp[2];

    for(int i = 0; i < anzelem; i++)
    {

        if(i == (p - 1) && i != size)
        {
            char tchar(sp[i]);
            if(r == 'o')
            {
                if(i == (anzelem - 1)) return false;
                sp[i] = sp[i+1];
                sp[i+1] = tchar;
                return true;
            }
            else
            {
                if(i == 0) return false;
                sp[i] = sp[i-1];
                sp[i-1] = tchar;
                return true;
            }
        }
    }
}

int main()
{

    stapel test(2);

    test.push('a');
    test.push('b');
    test.push('c');
    /*cout << test.pop() << "\n";
    cout << test.pop() << "\n";*/
    //if(!test.swap(2,'o')) cout << "Mhh, doof\n";
    cout << test.pop() << "\n";
    cout << test.pop() << "\n";
    cout << test.pop() << "\n";
    return 0;
}
