#include <iostream>

using namespace std;

class Fahrzeug
{
private:
    int fgn;
    float maxv;
    float actv;

public:
    Fahrzeug(int nr, float v);
    void print();
    void print_actv();
    void accelerate(float a);
};

Fahrzeug::Fahrzeug(int nr, float v)
{
    this->fgn = nr;
    this->maxv = v;
    this->actv = 0;
}

void Fahrzeug::print()
{
    cout << "FGN: " << this->fgn << " MAX_V: " << this->maxv << " ACT_V: " << this->actv << " \n";
}

void Fahrzeug::accelerate(float a)
{
    if((a + this->actv) > this->maxv) return;
    if((a + this->actv) < 0) this->actv = 0;
    this->actv += a;
}

int main()
{
    Fahrzeug roller(2, 50);
    roller.print();


    return 0;
}
