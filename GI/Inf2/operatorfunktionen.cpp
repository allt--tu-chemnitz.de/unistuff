#include <iostream>
#include <fstream>

using namespace std;

class blub
{
private:
    int moin;
public:
    blub();
    blub(int b);
    int get_moin();
    void set_moin(int b);
    int operator+(blub &A);
    blub operator-(blub &A);
    bool operator==(blub &A);
    void operator*=(int b);
    friend ostream& operator<<(ostream stream, const blub &A);
    friend istream& operator>>(istream stream, const blub &A);
};

blub::blub()
{
    moin = 0;
}

blub::blub(int b)
{
    moin = b;
}

int blub::get_moin()
{
    return moin;
}

void blub::set_moin(int b)
{
    moin = b;
}

int blub::operator+(blub &A)
{
    return (A.get_moin() + this->moin);
}

blub blub::operator-(blub &A)
{
    blub n(this->moin - A.moin);
    return n;
}

bool blub::operator==(blub &A)
{
    if(this->moin == A.moin) return true;
    return false;
}

void blub::operator*=(int b)
{
    this->moin *= b;
    return;
}

ostream& operator<<(ostream &stream, blub &A)
{
    cout << A.get_moin() << "\n";
    return stream;
}

istream& operator>>(istream& stream, blub &A)
{

    char dat[50];
    cout << "Datei Eingeben: ";
    cin >> dat;

    ifstream data(dat);
    if(!data)
    {
        cout << "Datei nicht gefunden";
        return stream;
    }
    int i;
    data >> i;
    data.close();
    A.set_moin(i);
    return stream;

}

int main(void)
{
    blub A(4), B(8), G;

    blub c = A - B;

    cout << A+B << "\n";
    cout << c << "\n";

    if(c == B) cout << "Falsch\n";
    else cout << "Richtig\n";

    c *= 6;
    cout << c;
    cin >> G;
    cout << G;

    return 0;
}
