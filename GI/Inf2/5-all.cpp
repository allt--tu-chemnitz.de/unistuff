#include <iostream>

using namespace std;

struct dliste
{
    int data;
    dliste* next;
    dliste* previous;
};

class DList
{
    private:
        int size;
        dliste* head;
        dliste* tail;
    public:
        DList();
        ~DList();
        void print_clockwise();
        void print_cclockwise();
        int get_size();
        void insert_front(int d);
        void insert_back(int d);
        void remove_front();
        void remove_back();
        bool remove_data(int d);
        void bubblesort();
        //void quicksort(dliste* l, dliste* r);
        void clear();
};

DList::DList()
{
    this->size = 0;
    this->head = NULL;
    this->tail = NULL;
}

DList::~DList()
{
    while(this->head)
    {
        dliste* del = this->head;
        this->head = this->head->next;
        delete del;
    }
    this->size = 0;
    this->tail = NULL;
}

//5-3

void DList::print_clockwise()
{
    dliste* fwd = this->head;
    while(fwd)
    {
        cout << fwd->data << ", ";
        fwd = fwd->next;
    }
    cout << "\n";
}

void DList::print_cclockwise()
{
    dliste* bwd = this->tail;
    while(bwd)
    {
        cout << bwd->data << ", ";
        bwd = bwd->previous;
    }
    cout << "\n";
}

//5-4

int DList::get_size()
{
    return this->size;
}

//5-5

void DList::insert_front(int d)
{
    dliste* enew = new dliste;
    enew->previous = NULL;
    enew->next = this->head;
    if(this->head) this->head->previous = enew;
    else this->tail = enew;
    this->size++;
    this->head = enew;
    enew->data = d;
}

void DList::insert_back(int d)
{
    dliste* enew = new dliste;
    enew->next = NULL;
    enew->previous = this->tail;
    if(this->tail) this->tail->next = enew;
    else this->head = enew;
    this->size++;
    this->tail = enew;
    enew->data = d;
}

//5-6

void DList::remove_front()
{
    if(this->size)
    {
        if(this->size == 1)
        {
            delete this->head;
            this->head = this->tail = NULL;
        }
        else
        {
            dliste* del = head;
            head = head->next;
            head->previous = NULL;
            delete del;
        }

        this->size--;
    }
    else cout << "Liste leer\n";
}

void DList::remove_back()
{
    if(this->size)
    {
        if(this->size == 1)
        {
            delete this->head;
            this->head = this->tail = NULL;
        }
        else
        {
            dliste* del = tail;
            tail = tail->previous;
            tail->next = NULL;
            delete del;
        }

        this->size--;
    }
    else cout << "Liste leer\n";
}

//5-7

void DList::clear()
{
    this->~DList();

    while(this->head)
    {
        dliste* del = this->head;
        this->head = this->head->next;
        delete del;
    }
    this->size = 0;
    this->tail = NULL;
}

//5-9

bool DList::remove_data(int d)
{
    dliste* cnt = this->head;

    while(cnt)
    {
        if(cnt->data == d)
        {
            if(!cnt->previous)
            {
                this->remove_front();
                return true;
            }
            if(!cnt->next)
            {
                this->remove_back();
                return true;
            }
            if(cnt->next && cnt->previous)
            {
                cnt->previous->next = cnt->next;
                cnt->next->previous = cnt->previous;

                delete cnt;
                return true;
            }
        }
        else
        {
            cnt = cnt->next;
        }
    }
    return false;
}

//5-12

void DList::bubblesort()
{
    dliste* cnt = this->head;
    int i = this->size, counter = 0,tmp;

    while(i > 1)
    {
        cnt = this->head;
        while(cnt)
        {
            if(cnt->next)
            {
                if(cnt->data > cnt->next->data)
                {
                    tmp = cnt->data;
                    cnt->data = cnt->next->data;
                    cnt->next->data = tmp;
                }
            }
            cnt = cnt->next;
        }
        counter++;
        i--;
    }

}
//Needs even more work here
//Dran denken das oben auszukommentieren, wenn man das benutzen will (wenn es mal fertig ist)
/*void DList::quicksort(dliste* l = head, dliste* r = tail)
{
    dliste* cnt = l;
    while(cnt != r)
    {
        //Needs work
    }
}*/

int main()
{
    DList liste;

    //Irgendwelche Beispiele

    liste.insert_front(2);
    liste.insert_front(4);
    liste.insert_front(1);
    liste.insert_front(2);
    liste.print_clockwise();
    liste.print_cclockwise();
    cout << liste.get_size() << '\n';
    liste.remove_data(1);
    liste.print_clockwise();
    liste.bubblesort();
    liste.print_clockwise();

    return 0;
}
