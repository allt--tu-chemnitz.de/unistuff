#include <iostream>
#include <cmath>

using namespace std;

int main()
{

    int i;
    float s;

    cout << "Geben Sie die laenge des Feldes ein: ";
    cin >> i;

    int *f = new int[i];

    cout << "Geben Sie nun die Werte ein:\n";

    for(int j = 0; j<i; j++)
    {
        cin >> f[j];
        s += f[j] * f[j];
    }

    delete[] f;

    cout << "Ergebnis: " << sqrt(s) << "\n";


    return 0;
}
