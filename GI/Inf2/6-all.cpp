#include <iostream>
#include <string>

using namespace std;

struct artikel
{
    string type;
    int num;
    string name;
    float price;
    artikel* prev;
    artikel* next;
};

struct uap //user and price
{
    string name;
    float price;
    uap* next;
};

class einkauf
{
    private:
        int size;
        artikel* head;
        artikel* tail;
        float price_all;

    public:
        einkauf();
        ~einkauf();
        void print();
        void insert_front(string t, int m, string n, float p);
        bool remove_combo(string t, string n);
        float return_price()
        {
            return price_all;
        };
        float return_user_price(string n);
        uap* return_price_per_user();
};

einkauf::einkauf()
{
    size = 0;
    head = NULL;
    tail = NULL;
    price_all = 0.0;
}

einkauf::~einkauf()
{
    delete this;
}

float einkauf::return_user_price(string n)
{
    artikel* tmp(head);
    float sum(0.0);

    while(tmp)
    {
        if(tmp->name == n) sum += tmp->num * tmp->price;

        tmp = tmp->next;
    }

    return sum;
}

uap* einkauf::return_price_per_user()
{
    uap* kopf = NULL;
    uap* tmp_uap = kopf;
    artikel* tmp = head;

    while(tmp)
    {
        while(tmp_uap)
        {
            if(tmp->name == tmp_uap->name)
            {
                tmp_uap->price += tmp->num * tmp->price;
                break;
            }
            if(!tmp_uap->next)
            {
                uap* neu = new uap;
                neu->name = tmp->name;
                neu->price = tmp->num * tmp->price;
                kopf = neu;
                break;
            }

            tmp_uap = tmp_uap->next;
        }

        tmp_uap = kopf;
    }


    return kopf;
}

int main()
{

    return 0;
}
